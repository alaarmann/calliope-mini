# calliope-mini

_calliope-mini_ contains everything required getting started using Rust to create firmwares for the
[Calliope mini](https://www.calliope.cc) microcontroller board. This little board has a lot built-in,
even a capable debugging interface.

This Board Support Package (BSP) for the Calliope mini pursues the goal of making most features of the board easily available while still supporting advanced applications to exploit the full capabilities of the hardware. It should enable learners to make first steps in programming with confidence and at the same time experienced programmers to delve into the world of microcontrollers.

## Important notes

- this project is currently under active development, APIs are expected to change.
- it is a fork of [microbit](https://github.com/nrf-rs/microbit), a Board Support Package (BSP) for the BBC micro:bit
- using the crate you are _required_ to specify the version of the Calliope mini by activating the corresponding feature, e.g. `v1`

## Calliope mini version

For determining the version of your board see the [product page](https://calliope.cc/en/calliope-mini/tech-facts).

Calliope mini V1 and V2 are almost identical (apart from the coprocessor, its firmware and its flash memory), so this crate's `v2` feature is currently an alias for `v1`.

Compared to the BBC micro:bit's versions, Calliope v1 and V2 are similar to micro:bit V1, while Calliope mini V3 is similar to micro:bit V2.

## Getting started

All you need to start programming this device is:

- A Calliope mini board
- A computer: Linux is tested
- A bit of open source software

### Install dependencies

On Linux you have the options to use `cargo install` or `nix-shell`.

#### Cargo install

In order to run the examples you need to install [`flip-link`](https://github.com/knurling-rs/flip-link#installation) and [`probe-rs`](https://probe.rs/docs/tools/probe-rs/).

```bash
> cargo install probe-rs-tools flip-link
```

#### Nix

[Install the Nix package manager](https://nixos.org/download/) and start a Nix shell in the project's base directory

```bash
> nix-shell
```

### Run an example

The first thing to try is one of the [examples](./examples) in this repository. Plug in your Calliope mini and
run one of the commands below.

_For Calliope mini V1_

```bash
> cargo run --release --manifest-path ./examples/display-blocking/Cargo.toml --features v1 --target thumbv6m-none-eabi
```

_For Calliope mini V2_

```bash
> cargo run --release --manifest-path ./examples/display-blocking/Cargo.toml --features v2 --target thumbv6m-none-eabi
```

_For Calliope mini V3_

```bash
> cargo run --release --manifest-path ./examples/display-blocking/Cargo.toml --features v3 --target thumbv7em-none-eabihf
```

You should see a lot of build output, the orange LED on the back of the Calliope mini should flash quickly, and a message should appear on the LED display.

Congratulations! You've flashed your first Rust program onto your Calliope mini!

### Feature support matrix

| Feature            | Board version 1.x and 2.x | Board version 3.x |
| ------------------ | ------------------------- | ----------------- |
| LED matrix         | Supported                 | Supported         |
| RGB LED            | Example given             | Example given     |
| Beeper             | Example given             | Example given     |
| Buttons            | Supported                 | Supported         |
| Accelerometer      | No example yet            | No example yet    |
| Gyroscope          | No example yet            | n/a               |
| Magnetometer       | Example given             | Example given     |
| Sound sensor       | Example given             | Example given     |
| Temperature sensor | No example yet            | No example yet    |
| Light sensor       | No example yet            | No example yet    |
| Motor driver       | No example yet            | No example yet    |

## License

[MIT](LICENSE)
