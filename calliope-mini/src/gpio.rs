//! Named GPIO pin types
//!
//! This module maps the GPIO pin names as described in the
//! [schematics of the Calliope mini site](https://docs.calliope.cc/tech/hardware/datenblatt/)
//! Where appropriate the pins are restricted with the appropriate `MODE`
//! from `nrf-hal`.
#[cfg(feature = "v1")]
pub use crate::v1::gpio::*;

#[cfg(feature = "v3")]
pub use crate::v3::gpio::*;
