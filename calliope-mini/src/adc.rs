//! ADC
#[cfg(feature = "v1")]
pub use crate::v1::adc::*;

#[cfg(feature = "v3")]
pub use crate::v3::adc::*;
