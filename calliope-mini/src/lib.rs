//! calliope-mini contains everything required to get started with the use of Rust
//! to create firmwares for the [Calliope mini](https://calliope.cc)
//! microcontroller board.
#![doc(html_root_url = "https://docs.rs/crate/calliope-mini/latest")]
#![no_std]
#![deny(missing_docs)]
#![allow(non_camel_case_types)]

#[cfg(all(feature = "v1", feature = "v3"))]
compile_error!("cannot build for calliope-mini v1 and v3 at the same time");

#[cfg(feature = "v1")]
pub use nrf51_hal as hal;

#[cfg(feature = "v3")]
pub use nrf52833_hal as hal;

pub use hal::pac;
pub use hal::pac::Peripherals;

pub mod adc;
pub mod board;
pub mod display;
pub mod gpio;

pub use board::Board;

#[cfg(feature = "v1")]
mod v1;

#[cfg(feature = "v3")]
mod v3;
