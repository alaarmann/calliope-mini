# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2025-01-24

### Added

- Support Calliope mini v2 as alias for v1
- Export remaining nRF51 devices in struct Board (v1)

## [0.2.1] - 2025-01-01

### Changed

- Fix README
- Clean up example `rgb-led-v1`

## [0.2.0] - 2024-12-30

### Added

- Add CHANGELOG
- Support Calliope mini v3
- Rework examples
- Add MSRV

### Changed

- Update README
- Run with `probe-rs run`
- Upgrade `embedded-hal` to `1.0.0`
- Upgrade Rust to `1.79.0`

## [0.1.0] - 2024-03-30

### Changed

- Restructure project, keep only one single crate `calliope-mini`.
- Adapt `microbit` to `calliope-mini`, support `v1`

[unreleased]: https://gitlab.com/alaarmann/calliope-mini/-/compare/v0.3.0..HEAD
[0.3.0]: https://gitlab.com/alaarmann/calliope-mini/-/tree/v0.3.0
[0.2.1]: https://gitlab.com/alaarmann/calliope-mini/-/tree/v0.2.1
[0.2.0]: https://gitlab.com/alaarmann/calliope-mini/-/tree/v0.2.0
[0.1.0]: https://gitlab.com/alaarmann/calliope-mini/-/tree/v0.1.0
