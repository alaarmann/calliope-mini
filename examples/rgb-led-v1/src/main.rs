#![no_main]
#![no_std]

use defmt_rtt as _;
use panic_halt as _;

use calliope_mini::{
    board::Board,
    hal::{gpio::Level, Timer},
};
use cortex_m_rt::entry;
use embedded_hal::delay::DelayNs;
use smart_leds::{brightness, SmartLedsWrite, RGB8};
use ws2812_nop_nrf51::Ws2812;

#[entry]
fn main() -> ! {
    let board = Board::take().unwrap();
    let mut timer = Timer::new(board.TIMER0);
    let rgb_led_pin = board.rgb_led_pin.into_push_pull_output(Level::Low);

    let mut ws2812: Ws2812 = Ws2812::new(rgb_led_pin.into());

    let leds = [
        RGB8::new(255, 0, 0),
        RGB8::new(0, 255, 0),
        RGB8::new(0, 0, 255),
        RGB8::new(255, 255, 0),
        RGB8::new(0, 255, 255),
        RGB8::new(255, 0, 255),
        RGB8::new(20, 20, 20),
        RGB8::new(0, 0, 0),
    ];

    defmt::info!("starting");

    let nleds = leds.len();
    let mut start = 0;
    loop {
        let cur_leds: [RGB8; 1] = [leds[start]];
        defmt::debug!("before write");
        ws2812
            .write(brightness(cur_leds.iter().cloned(), 8))
            .unwrap();
        defmt::debug!("written");
        timer.delay_ms(500);
        start = (start + 1) % nleds;
    }
}
