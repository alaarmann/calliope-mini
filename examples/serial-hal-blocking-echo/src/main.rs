#![no_main]
#![no_std]

use defmt_rtt as _;
use panic_halt as _;

use core::fmt::Write;
use cortex_m_rt::entry;
use embedded_io::Read;

#[cfg(feature = "v1")]
use calliope_mini::{
    hal::uart,
    hal::uart::{Baudrate, Parity},
};

#[cfg(feature = "v3")]
use calliope_mini::{
    hal::uarte,
    hal::uarte::{Baudrate, Parity},
};

#[cfg(feature = "v3")]
mod serial_setup;
#[cfg(feature = "v3")]
use serial_setup::UartePort;

#[entry]
fn main() -> ! {
    let board = calliope_mini::Board::take().unwrap();

    #[cfg(feature = "v1")]
    let mut serial = {
        uart::Uart::new(
            board.UART0,
            board.uart.into(),
            Parity::EXCLUDED,
            Baudrate::BAUD115200,
        )
    };

    #[cfg(feature = "v3")]
    let mut serial = {
        let serial = uarte::Uarte::new(
            board.UARTE0,
            board.uart.into(),
            Parity::EXCLUDED,
            Baudrate::BAUD115200,
        );
        UartePort::new(serial)
    };

    loop {
        write!(serial, "Hello World:\r\n").unwrap();
        let mut input = [0];
        serial.read_exact(&mut input).unwrap();
        write!(serial, "You said: {}\r\n", input[0] as char).unwrap();
    }
}
