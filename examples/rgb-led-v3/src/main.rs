#![no_main]
#![no_std]

use embedded_hal::delay::DelayNs;
use smart_leds::{brightness, SmartLedsWrite, RGB8};
use ws2812_nrf52833_pwm::Ws2812;

use defmt_rtt as _;
use panic_halt as _;

use calliope_mini::{board::Board, hal::Timer};
use cortex_m_rt::entry;

#[entry]
fn main() -> ! {
    let board = Board::take().unwrap();
    let mut timer = Timer::new(board.TIMER0);
    let pin = board.rgb_led_pin.degrade();
    let mut ws2812: Ws2812<{ 3 * 24 }, _> = Ws2812::new(board.PWM0, pin);

    let leds = [
        RGB8::new(255, 0, 0),
        RGB8::new(0, 255, 0),
        RGB8::new(0, 0, 255),
        RGB8::new(255, 255, 0),
        RGB8::new(0, 255, 255),
        RGB8::new(255, 0, 255),
        RGB8::new(20, 20, 20),
        RGB8::new(0, 0, 0),
    ];

    defmt::info!("starting");

    ws2812.write(brightness(leds.iter().cloned(), 32)).unwrap();

    defmt::debug!("displaying indices");

    timer.delay_ms(3000);

    defmt::debug!("starting loop");

    let nleds = leds.len();
    let mut start = 0;
    loop {
        let mut cur_leds: [RGB8; 3] = Default::default();
        for i in 0..cur_leds.len() {
            cur_leds[i] = leds[(i + start) % nleds];
        }
        let tmp = cur_leds[0];
        cur_leds[0] = RGB8::new(255, 255, 255);
        ws2812
            .write(brightness(cur_leds.iter().cloned(), 32))
            .unwrap();

        defmt::debug!("tick");
        cur_leds[0] = tmp;
        ws2812
            .write(brightness(cur_leds.iter().cloned(), 32))
            .unwrap();
        defmt::debug!(".");
        timer.delay_ms(500);
        start = (start + 1) % nleds;
    }
}
